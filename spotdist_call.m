


function spotdist_call()
%% load the files produced by the jython program (Fiji), that are containing the experimental spot positions
% in 2 canals in 2D. Unix computers only


clear;
close all;

% isOpen = matlabpool('size');
% if isOpen==0
%     matlabpool(8);
% end

%% jython produced file
folderName = uipickfiles('REFilter','_results','REDirs',0,'Prompt','Select _results folders');
%folderName{1} = '/media/Disk_3T_seb/data2/Analyses_zstacks/140214_237_stream_1_crops/Fiji_Fused 140213_zs_YPT237_RFPGFP_stream_1/Fused 140213_zs_YPT237_RFPGFP_stream_1_results';

header = {'X1' 'Y1' 'SNR1' 'X2' 'Y2' 'SNR2'}; % to be adapted if the format of spotPosition changes
minSNR = 5.0;

for FOV=1:length(folderName) % for each FOV
	close all
	cd(folderName{FOV});
	channels = dir('results_channel*');
	for chan = 1:length(channels) % for each channel
		if isempty(regexp(channels(chan).name, '.*analysis*','ONCE'))
			cd(folderName{FOV})
			cd(channels(chan).name)
			[spotPos1canal roiNumber]= extract_position(strcat(folderName{FOV},'/',channels(chan).name)); % in um
			spotPosition(:,(chan*3)-2:chan*3) = spotPos1canal;
			clear  spotPos1canal
		end
	end
	[goodRoiNumber goodPosition badRoi] = deleteNoise(spotPosition,roiNumber,minSNR);
	spotDistance = calculateDistances(goodPosition); % calculate distances + get rid of roi without only one spot
	
	% calculate 3D distances
	spotDistance3D={};
	spotDistance3D.all = spotDistance*sqrt(3/2); % estimated projection of the spatial distance in 3D (assumes isotropy)
	spotDistance3D.mean = mean(spotDistance3D.all);
	spotDistance3D.std = std(spotDistance3D.all);
	spotDistance3D.SEM = spotDistance3D.std/length(spotDistance3D.all);
	spotDistance3D.median = median(spotDistance3D.all);
	spotDistance3D.iqr = iqr(spotDistance3D.all);
	
	spotDistance2D={};
	spotDistance2D.all = spotDistance; 
	spotDistance2D.mean = mean(spotDistance2D.all);
	spotDistance2D.std = std(spotDistance2D.all);
	spotDistance2D.SEM = spotDistance2D.std/length(spotDistance2D.all);
	spotDistance2D.median = median(spotDistance2D.all);
	spotDistance2D.iqr = iqr(spotDistance2D.all);
	
	
	
	cd ..
	if exist('results_analysis','dir')==7
		rmdir('results_analysis','s')
	end
	mkdir('results_analysis')
	cd results_analysis/
 	save('summary_spots','spotDistance2D','goodRoiNumber','goodPosition','badRoi','header','spotDistance3D');
	analyseDistances(goodPosition,spotDistance);
% 	sprintf('median = %s, minSNR = %d\n', median(spotDistance2D.all), minSNR)
	clear spotPosition*
end

end


function [spotPosition roiNumber] = extract_position(folderName)
cd(folderName)
filesInFolder = dir('*_data.txt');

roiNumber = {};
for i = 1:length(filesInFolder)
	clear datas
	datas = importdata(filesInFolder(i).name);
	roiNumber{i} = regexprep(filesInFolder(i).name,'yeast_ROI','roi ');
	roiNumber{i} = regexprep(roiNumber{i},'_data.txt','');
	spotPosition(i,1:2) = datas.data(1:2); % keeps only the x,y positions of the locus
	spotPosition(i,3) = datas.data(8); % as well as its SNRmax Value
end

settings = importdata('condition_settings.txt');
pixelSize = settings.data(1)/1000; % output is in um
spotPosition(:,1:2) = spotPosition(:,1:2)*pixelSize;
end


function [goodRoiNumber goodPosition badRoi] = deleteNoise(spotPosition,roiNumber,minSNR)
% spotDistance = (dist,roinumber)

goodRoi = 0;
bothcanals = 0;
canal1 = 0;
canal2 = 0;
badRoi = {};
for roi = 1:length(roiNumber)
	if (spotPosition(roi,3)>minSNR && spotPosition(roi,6)>minSNR)
		goodRoi = goodRoi+1;
		goodPosition(goodRoi,:) = spotPosition(roi,:);
		goodRoiNumber{goodRoi} = roiNumber{roi};
	elseif (spotPosition(roi,3)<=minSNR && spotPosition(roi,6)<=minSNR)
		bothcanals = bothcanals+1;
		badRoi.bothCanals.spotPosition{bothcanals} = spotPosition(roi,:);
		badRoi.bothCanals.roiNumber{bothcanals} = roiNumber{roi};
	elseif spotPosition(roi,3)<=minSNR
		canal1 = canal1+1;
		badRoi.canal1.spotPosition{canal1} = spotPosition(roi,:);
		badRoi.canal1.roiNumber{canal1} = roiNumber{roi};
	elseif spotPosition(roi,6)<=minSNR
		canal2 = canal2+1;
		badRoi.canal2.spotPosition{canal2} = spotPosition(roi,:);
		badRoi.canal2.roiNumber{canal2} = roiNumber{roi};
	end
end

end


function spotDistance = calculateDistances(goodPosition)

for roi = 1:length(goodPosition)
	spotDistance(roi) = sqrt((goodPosition(roi,1)-goodPosition(roi,4))^2+...
		(goodPosition(roi,2)-goodPosition(roi,5))^2);
end

end

function analyseDistances(goodPosition,spotDistance)

% boxplot of the individual axis dispersion
DeltaX = goodPosition(:,1)-goodPosition(:,4);
DeltaY = goodPosition(:,2)-goodPosition(:,5);
leg{1} = 'DeltaX';
leg{2} = 'DeltaY';
figure; boxplot([DeltaX,DeltaY],leg);
title('X and Y axis dispersion'); xlabel('Axis'); ylabel('spotDistance(um)');
leg2{1} = sprintf('medianX = %i(nm)\tiqrX = %i(nm)',round(median(DeltaX)*1000),round(iqr(DeltaX)*1000));
leg2{2} = sprintf('medianY = %i(nm)\tiqrY = %i(nm)',round(median(DeltaY)*1000),round(iqr(DeltaY)*1000));
textbp(leg2);
saveas(gcf,strcat('XY_dispersion.png'));
saveas(gcf,strcat('XY_dispersion.fig'));

% subplot of the cfd and hist of the distances
figure;
subplot(2,1,1);
cdfplot(spotDistance)
xlabel('2D distance (mu)'); ylabel('counts');
title(sprintf('Median 2D distance = %.3fum\tiqr = %.3fum\tN = %i cells',median(spotDistance),iqr(spotDistance),length(spotDistance)))

subplot(2,1,2);
[normDist,binning] = hist(spotDistance,0:0.1:1.5);
bar(binning,normDist/length(spotDistance));
axis([0 1.5 0 0.4])
xlabel('2D distance (mu)'); ylabel('Normalized count');
saveas(gcf,strcat('Distance_2D.png'));
saveas(gcf,strcat('Distance_2D.fig'));
end






